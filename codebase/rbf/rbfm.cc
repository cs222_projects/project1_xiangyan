#include <iostream>
#include <cstring>

#include "rbfm.h"

RecordBasedFileManager* RecordBasedFileManager::_rbf_manager = 0;

RecordBasedFileManager* RecordBasedFileManager::instance()
{
    if(!_rbf_manager)
        _rbf_manager = new RecordBasedFileManager();

    return _rbf_manager;
}

RecordBasedFileManager::RecordBasedFileManager()
{
}

RecordBasedFileManager::~RecordBasedFileManager()
{
	if(_rbf_manager)
	{
		delete _rbf_manager;
		_rbf_manager = 0;
	}
}

RC RecordBasedFileManager::createFile(const string &fileName) {
	PagedFileManager* pfm = PagedFileManager::instance();
	if(pfm->createFile(fileName.c_str()) != rc::OK)
	{
		return rc::FILE_CREATE_FAILED;
	}
	// Initialize the space
	FileHandle fileHandle;
	pfm->openFile(fileName.c_str(), fileHandle);
	initializePage(fileHandle, 0);
	return rc::OK;
}

RC RecordBasedFileManager::destroyFile(const string &fileName) {
	PagedFileManager* pfm = PagedFileManager::instance();
	return pfm->destroyFile(fileName.c_str());
}

RC RecordBasedFileManager::openFile(const string &fileName, FileHandle &fileHandle) {
	PagedFileManager* pfm = PagedFileManager::instance();
	return pfm->openFile(fileName.c_str(), fileHandle);
}

RC RecordBasedFileManager::closeFile(FileHandle &fileHandle) {
	PagedFileManager* pfm = PagedFileManager::instance();
	return pfm->closeFile(fileHandle);
}

RC RecordBasedFileManager::insertRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, RID &rid) {
	// Calculate the data length
	int dataSize = 0;
	char* curData = (char*)data;
	for(vector<Attribute>::const_iterator itr = recordDescriptor.begin(); itr != recordDescriptor.end(); itr++)
	{
		Attribute attr = *itr;
		switch(attr.type)
		{
		case TypeInt:
		case TypeReal:
			dataSize += 4;
			curData += 4;
			break;
		case TypeVarChar:
			int* varCharLength = (int*)curData;
			curData += 4;
			dataSize += 4;
			curData += *varCharLength;
			dataSize += *varCharLength;
			break;
		}
	}
	int totalDataSpace = PAGE_SIZE - SLOT_PER_PAGE*8;
	if(dataSize + sizeof(RID) > totalDataSpace)
	{
		return rc::RECORD_TOO_LARGE;
	}
	// Look for space in the current page
	int pageNumber = fileHandle.getNumberOfPages();
	bool isPutIn;
	for(int i = 0; i < pageNumber; i++)
	{
		isPutIn = false;
		void* aPage = malloc(PAGE_SIZE);
		fileHandle.readPage(i, aPage);
		char* calPage = (char*)aPage;
		char* charSlotDirectory = calPage + PAGE_SIZE - sizeof(SlotHeader) - sizeof(SlotDirectory);
		SlotDirectory* slotDirectory = (SlotDirectory*)charSlotDirectory;
		char* charSlotHeader = calPage + PAGE_SIZE - sizeof(SlotHeader);
		SlotHeader* slotHeader = (SlotHeader*)charSlotHeader;
		for(int slotNum = 1; slotNum < SLOT_PER_PAGE; slotNum++)
		{
			if(slotDirectory->offset == 0)
			{
				if(slotDirectory->length == 0)
				{
					// Empty slot
					if(slotNum == 1 && slotDirectory->length == 0)
					{
						// The first slot
						writeRecordToPage(calPage, i, data, dataSize, 0, slotDirectory, slotHeader,
								rid, slotNum, dataSize+sizeof(RID));
						isPutIn = true;
						break;
					}
					if(slotNum != 1)
					{
						int offsetNow = slotHeader->freeSpacePointer;
						// Calculate whether space is enough
						if(totalDataSpace - offsetNow - sizeof(RID) >= dataSize)
						{
							// Space is enough to put in data
							writeRecordToPage(calPage, i, data, dataSize, offsetNow, slotDirectory,
									slotHeader, rid, slotNum, offsetNow+dataSize+sizeof(RID));
							isPutIn = true;
							break;
						} else
						{
							// Space is not enough, next page
							break;
						}
					}
				} else if(slotDirectory->length != 0 && slotNum != 1)
				{
					if(slotDirectory->length >= dataSize + sizeof(RID))
					{
						// In this case the slot once had some data but was removed out
						int offsetNow;
						SlotDirectory* lastSlot = slotDirectory + 1;
						offsetNow = lastSlot->offset + lastSlot->length;
						writeRecordToPage(calPage, i, data, dataSize, offsetNow, slotDirectory,
								slotHeader, rid, slotNum, offsetNow+dataSize+sizeof(RID));
						isPutIn = true;
						break;
					} else
					{
						break;
					}
				}
				{

				}
			}
			// Change slot
			slotDirectory--;
		}
		if(isPutIn)
		{
			// Write back to disk
			fileHandle.writePage(i, aPage);
			free(aPage);
			break;
		}
		free(aPage);
	}
	if(!isPutIn)
	{
		// No space in existed pages, create new page
		void* newPage = malloc(PAGE_SIZE);
		fileHandle.appendPage(newPage);
		free(newPage);
		initializePage(fileHandle, pageNumber);
		void* aPage = malloc(PAGE_SIZE);
		fileHandle.readPage(pageNumber, aPage);
		char* calPage = (char*)aPage;
		char* charSlotDirectory = calPage + PAGE_SIZE - sizeof(SlotHeader) - sizeof(SlotDirectory);
		SlotDirectory* slotDirectory = (SlotDirectory*)charSlotDirectory;
		char* charSlotHeader = calPage + PAGE_SIZE - sizeof(SlotHeader);
		SlotHeader* slotHeader = (SlotHeader*)charSlotHeader;
		writeRecordToPage(calPage, pageNumber, data, dataSize, 0,
				slotDirectory, slotHeader, rid, 1, dataSize+sizeof(RID));
		fileHandle.writePage(pageNumber, aPage);
		free(aPage);
	}
    return rc::OK;
}

RC RecordBasedFileManager::readRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, void *data) {
	void* aPage = malloc(PAGE_SIZE);
	int res = fileHandle.readPage(rid.pageNum, aPage);
	if(res != rc::OK)
	{
		return res;
	}
	char* calPage = (char*)aPage;
	char* charSlotDirectory = calPage + PAGE_SIZE - (rid.slotNum+1)*8;
	SlotDirectory* slotDirectory = (SlotDirectory*)charSlotDirectory;
	int offset = slotDirectory->offset;
	int length = slotDirectory->length;
	calPage += offset;
	RID* contentRid = (RID*)calPage;
	if(contentRid->pageNum == rid.pageNum && contentRid->slotNum == rid.slotNum)
	{
		calPage += 8;
		memcpy(data, calPage, length - 8);
		free(aPage);
	    return rc::OK;
	}
	return rc::RECORD_READ_ERR;
}

RC RecordBasedFileManager::printRecord(const vector<Attribute> &recordDescriptor, const void *data) {
	char* charData = (char*)data;
	for(vector<Attribute>::const_iterator itr = recordDescriptor.begin(); itr != recordDescriptor.end(); itr++)
	{
		Attribute attr = *itr;
		switch(attr.type)
		{
		case TypeInt:
			cout<<"name: "<<attr.name<<", type: int, attrLength: "<<attr.length<<", value: "<<*(int*)charData<<endl;
			charData += 4;
			break;
		case TypeReal:
			cout<<"name: "<<attr.name<<", type: real, attrLength: "<<attr.length<<", value: "<<*(float*)charData<<endl;
			charData += 4;
			break;
		case TypeVarChar:
			cout<<"name: "<<attr.name<<", type: varChar, attrLength: "<<attr.length<<", value: ";
			int* size = (int*)charData;
			charData += 4;
			for(int i = 0; i < *size; i++)
			{
				char temp = *charData;
				cout<<temp;
				charData++;
			}
			cout<<endl;
			break;
		}
	}
    return rc::OK;
}

RC RecordBasedFileManager::initializePage(FileHandle &fileHandle, int pageNum)
{
	void* aPage = malloc(PAGE_SIZE);
	char* pointer = (char*)aPage;
	pointer += (PAGE_SIZE - SLOT_PER_PAGE*8);
	vector<SlotDirectory> slots;
	for(int i = 0; i < SLOT_PER_PAGE - 1; i++)
	{
		SlotDirectory slot;
		slots.push_back(slot);
	}
	memcpy(pointer, &slots, SLOT_PER_PAGE*8);
	SlotHeader header;
	header.slotTotal = SLOT_PER_PAGE;
	pointer += (SLOT_PER_PAGE - 1)*8;
	memcpy(pointer, &header, 8);
	fileHandle.writePage(pageNum, aPage);
	free(aPage);
	return rc::OK;
}

RC RecordBasedFileManager::writeRecordToPage(char* aPage, int pageNum, const void* data, int dataSize, int offset,
		SlotDirectory* slotDirectory, SlotHeader* slotHeader, RID &rid, int slotNum, int pointOffset)
{
	rid.pageNum = pageNum;
	rid.slotNum = slotNum;
	memcpy(aPage+offset, &rid, sizeof(RID));
	memcpy(aPage+offset+sizeof(RID), data, dataSize);
	slotDirectory->offset = offset;
	slotDirectory->length = dataSize + sizeof(RID);
	slotHeader->freeSpacePointer = pointOffset;
	return rc::OK;
}
