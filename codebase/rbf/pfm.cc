#include "pfm.h"



PagedFileManager* PagedFileManager::_pf_manager = 0;

PagedFileManager* PagedFileManager::instance()
{
    if(!_pf_manager)
        _pf_manager = new PagedFileManager();

    return _pf_manager;
}


PagedFileManager::PagedFileManager()
{
}


PagedFileManager::~PagedFileManager()
{
	if(_pf_manager)
	{
		delete _pf_manager;
		_pf_manager = 0;
	}
}


RC PagedFileManager::createFile(const char *fileName)
{
	// Test whether fileName already exists
	FILE* file = fopen(fileName, "rb");
	if(file)
	{
		return rc::FILE_EXISTS;
	}
	// Create file
	file = fopen(fileName, "wb");
	if(!file)
	{
		return rc::FILE_CREATE_FAILED;
	}
    return rc::OK;
}


RC PagedFileManager::destroyFile(const char *fileName)
{
	// Test whether filName already exists
	FILE* file = fopen(fileName, "rb");
	if(!file)
	{
		return rc::FILE_NOT_FOUND;
	}
	fclose(file);
	// Discard the content and close it
	int res = remove(fileName);
	if(res != 0)
	{
		return rc::FILE_REMOVE_FAILED;
	}
    return rc::OK;
}


RC PagedFileManager::openFile(const char *fileName, FileHandle &fileHandle)
{
	// Test whether file has been created
	FILE* file = fopen(fileName, "rb");
	if(!file)
	{
		return rc::FILE_NOT_FOUND;
	}
	fclose(file);
	// Open the file and load to fileHandle
	file = fopen(fileName, "rb+");
	fileHandle.loadFile(file);
    return rc::OK;
}


RC PagedFileManager::closeFile(FileHandle &fileHandle)
{
	if(&fileHandle == NULL)
	{
		return rc::FILEHANDLE_NOT_FOUND;
	}
	fileHandle.closeFile();
    return rc::OK;
}


FileHandle::FileHandle():_file(NULL)
{
}


FileHandle::~FileHandle()
{
	if(_file)
	{
		_file = NULL;
	}
}


RC FileHandle::readPage(PageNum pageNum, void *data)
{
	if(pageNum+1 > getNumberOfPages())
	{
		return rc::PAGE_NUMBER_EXCEEDS;
	}
	int seekRes = fseek(_file, pageNum * PAGE_SIZE, SEEK_SET);
	if(seekRes != 0)
	{
		return rc::PAGE_SEEK_ERR;
	}
	size_t readSize = fread(data, PAGE_SIZE, 1, _file);
	if(readSize != 1)
	{
		return rc::PAGE_READ_ERR;
	}
    return rc::OK;
}


RC FileHandle::writePage(PageNum pageNum, const void *data)
{
	if(pageNum+1 > getNumberOfPages())
	{
		return rc::PAGE_NUMBER_EXCEEDS;
	}
	int seekRes = fseek(_file, pageNum * PAGE_SIZE, SEEK_SET);
	if(seekRes != 0)
	{
		return rc::PAGE_SEEK_ERR;
	}
	size_t writeSize = fwrite(data, PAGE_SIZE, 1, _file);
	if(writeSize != 1)
	{
		return rc::PAGE_WRITE_ERR;
	}
    return rc::OK;
}


RC FileHandle::appendPage(const void *data)
{
	int seekRes = fseek(_file, 0, SEEK_END);
	if(seekRes != 0)
	{
		return rc::PAGE_SEEK_ERR;
	}
	size_t writeSize = fwrite(data, PAGE_SIZE, 1, _file);
	if(writeSize != 1)
	{
		return rc::PAGE_WRITE_ERR;
	}
    return rc::OK;
}


unsigned FileHandle::getNumberOfPages()
{
	int seekRes = fseek(_file, 0, SEEK_END);
	if(seekRes != 0)
	{
		return rc::PAGE_SEEK_ERR;
	}
	return ftell(_file)/PAGE_SIZE;
}


RC FileHandle::loadFile(FILE* file)
{
	if(file)
	{
		_file = file;
		return rc::OK;
	}
	return rc::LOAD_FILE_FAILED;
}


RC FileHandle::closeFile()
{
	if(!_file)
	{
		return rc::CLOSE_FILE_FAILED;
	}
	int closeRes = fclose(_file);
	if(closeRes != 0)
	{
		return rc::CLOSE_FILE_FAILED;
	}
	return rc::OK;
}
