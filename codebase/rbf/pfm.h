#ifndef _pfm_h_
#define _pfm_h_

#include <stdio.h>

typedef int RC;
typedef unsigned PageNum;

#define PAGE_SIZE 4096

namespace rc
{
	enum ReturnCode
	{
		OK = 0,

		FILE_EXISTS = 1,
		FILE_CREATE_FAILED = 2,
		FILE_NOT_FOUND = 3,
		FILEHANDLE_NOT_FOUND = 4,
		FILE_REMOVE_FAILED = 5,

		PAGE_READ_ERR = 6,
		PAGE_SEEK_ERR = 7,
		PAGE_NUMBER_EXCEEDS = 8,
		PAGE_WRITE_ERR = 9,
		LOAD_FILE_FAILED = 10,
		CLOSE_FILE_FAILED = 11,

		RECORD_TOO_LARGE = 12,
		RECORD_READ_ERR = 13
	};
}

class FileHandle;


class PagedFileManager
{
public:
    static PagedFileManager* instance();                     // Access to the _pf_manager instance

    RC createFile    (const char *fileName);                         // Create a new file
    RC destroyFile   (const char *fileName);                         // Destroy a file
    RC openFile      (const char *fileName, FileHandle &fileHandle); // Open a file
    RC closeFile     (FileHandle &fileHandle);                       // Close a file

protected:
    PagedFileManager();                                   // Constructor
    ~PagedFileManager();                                  // Destructor

private:
    static PagedFileManager *_pf_manager;
};


class FileHandle
{
public:
    FileHandle();                                                    // Default constructor
    ~FileHandle();                                                   // Destructor

    RC readPage(PageNum pageNum, void *data);                           // Get a specific page
    RC writePage(PageNum pageNum, const void *data);                    // Write a specific page
    RC appendPage(const void *data);                                    // Append a specific page
    unsigned getNumberOfPages();                                        // Get the number of pages in the file

    RC loadFile(FILE* file);
    RC closeFile();

private:
    FILE* _file;
 };

 #endif
